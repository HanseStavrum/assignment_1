package com.assingment_1;

public class Main {

    public static void main(String[] args) {
	// write your code here
    }
}
/*
    We need:
    Character
        Mage - 5v, 1str, 1dex, 8 intelligence
            perlevel: 3, 1, 1, 5
        Ranger - 8, 1, 7, 1
            perlevel: 2, 1, 5, 1
        Rogue - 8, 2, 6, 1
            perlevel: 3, 1, 4, 1
        Warrior - 10, 5, 2, 1
            perlevel - 5, 3, 2, 1
        - Name - constructor
        - Level - init 1
        - Base Primary attributes
        - Total primary attributes

    Attributes
        Strength, Dexterity, Intelligence, Vitality

    Equipment
        Weapon
        Armor
                - InvalidArmorException
            Cloth - Mage
            Leather - Rogue, Ranger
            Mail - Rogue, Ranger
            Plate - Mail, Plate
                Head
                Body
                Legs


 */
