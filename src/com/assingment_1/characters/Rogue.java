package com.assingment_1.characters;

import com.assingment_1.items.Item;
import com.assingment_1.items.Weapon;

public class Rogue extends Character{
    public Rogue(String name) {
        super(name, 8, 2, 6, 1);
        allowedTypes.add(Item.ITEM_TYPE.LEATHER);
        allowedTypes.add(Item.ITEM_TYPE.MAIL);
        allowedWeapons.add(Weapon.WEAPON_TYPE.DAGGER);
        allowedWeapons.add(Weapon.WEAPON_TYPE.SWORD);
    }


    // increment this characters stats by params
    public void levelUp() {
        super.levelUp(3,1,4,1);
    }

    @Override
    public int getPrimaryAttribute() {
        return this.getTotalAttributes().getDexterity();
    }

    @Override
    public String toString() {
        return "rogue";
    }
}