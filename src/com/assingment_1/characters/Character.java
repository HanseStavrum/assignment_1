package com.assingment_1.characters;

import com.assingment_1.attributes.*;
import com.assingment_1.items.Armor;
import com.assingment_1.items.Item;
import com.assingment_1.items.Weapon;
import com.assingment_1.items.exceptions.InvalidArmorException;
import com.assingment_1.items.exceptions.InvalidWeaponException;

import java.util.ArrayList;
import java.util.HashMap;

// Contains name, Attributes, level, primaryStat
// functions for leveling up
public abstract class Character {
    private String name;
    private Attributes baseAttributes; // base stats
    private Attributes itemAttributes; // gained from items
    private Attributes totalAttributes = new Attributes(0,0,0,0); // combined
    int level = 1; // starting at level 1
    double dps = 1;


    // enum key, Item value
    HashMap<Item.SLOT, Item> itemMap = new HashMap<Item.SLOT, Item>();
    ArrayList<Item.ITEM_TYPE> allowedTypes = new ArrayList<>();
    ArrayList<Weapon.WEAPON_TYPE> allowedWeapons = new ArrayList<>();

    // starting stats as params
    public Character(String name, int v, int s, int d, int i) {
        this.name = name;
        this.baseAttributes = new Attributes(v, s, d, i);
        this.itemAttributes = new Attributes(0,0,0,0);
        this.totalAttributes = new Attributes(v,s,d,i);
        itemMap.put(Item.SLOT.HEAD, null);
        itemMap.put(Item.SLOT.BODY, null);
        itemMap.put(Item.SLOT.LEGS, null);
        itemMap.put(Item.SLOT.WEAPON, null);
    }

    // removes attributes gained from old item
    // equips new item and adds new attributes
    public boolean equip(Item item) throws InvalidArmorException, InvalidWeaponException {
        if (item.getClass() == Armor.class) {
            return equipArmor(item);
        }

        if (item.getClass() == Weapon.class) {
            return equipWeapon( (Weapon)item );
        }

        return false;
    }

    // removes attributes gained from old item
    // equips new item and adds new attributes
    public boolean equipArmor(Item item) throws InvalidArmorException {
        // checks level
        if (item.getRequiredLevel() > this.level) {
            throw new InvalidArmorException("Character is too low level to equip item");
        }

        // checks type, ex: CLOTH, LEATHER...
        if ( allowedTypes.contains(item.getItem_type()) == false ) {
            throw new InvalidArmorException("Character cannot equip that Item-Type");
        }

        Item.SLOT slot = item.getSlot();
        Item currentlyEquipped = itemMap.get(slot);
        if (currentlyEquipped != null) {
            Attributes currentlyAddedAttributes = currentlyEquipped.getAttributes();
            itemAttributes.removeAttributes(currentlyAddedAttributes);
            totalAttributes.removeAttributes(currentlyAddedAttributes);
        }

        itemAttributes.addAttributes(item.getAttributes());
        totalAttributes.addAttributes(item.getAttributes());
        itemMap.put(item.getSlot(), item);
        return true;
    }

    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        // checks level
        if (weapon.getRequiredLevel() > this.level) {
            throw new InvalidWeaponException("Character is too low level to equip item");
        }

        if ( allowedWeapons.contains(weapon.getWeapon_type()) == false ) {
            throw new InvalidWeaponException("Character cannot equip that Weapon-Type");
        }

        itemAttributes.addAttributes(weapon.getAttributes());
        totalAttributes.addAttributes(weapon.getAttributes());
        itemMap.put(weapon.getSlot(), weapon);
        return true;
    }

    // takes increments as parameter
    public void levelUp(int v, int s, int d, int i) {
        this.level += 1;
        int vitality = this.baseAttributes.getVitality();
        int strength = this.baseAttributes.getStrength();
        int dexterity = this.baseAttributes.getDexterity();
        int intelligence = this.baseAttributes.getIntelligence();

        baseAttributes.setVitality(vitality + v);
        baseAttributes.setStrength(strength + s);
        baseAttributes.setDexterity(dexterity + d);
        baseAttributes.setIntelligence(intelligence + i);
    }

    public String getName() {
        return name;
    }

    public Attributes getBaseAttributes() {
        return baseAttributes;
    }

    public Attributes getItemAttributes() {
        return itemAttributes;
    }

    public Attributes getTotalAttributes() {
        return totalAttributes;
    }

    public int getLevel() {
        return level;
    }

    public abstract int getPrimaryAttribute();

    public double calculateDps() {
        Weapon weapon = (Weapon)itemMap.get(Item.SLOT.WEAPON);
        if (weapon == null) {
            return 1;
        }

        int weaponDps = weapon.getDps();
        int mainStat = this.getPrimaryAttribute();
        dps = weaponDps * (1 + mainStat / 100.0);
        return dps;
    }

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", " + totalAttributes +
                ", dps=" + calculateDps() +
                '}';
    }
}


/*
Character
        Mage - 5v, 1str, 1dex, 8 intelligence
        perlevel: 3, 1, 1, 5
        Ranger - 8, 1, 7, 1
        perlevel: 2, 1, 5, 1
        Rogue - 8, 2, 6, 1
        perlevel: 3, 1, 4, 1
        Warrior - 10, 5, 2, 1
        perlevel - 5, 3, 2, 1
        - Name - constructor
        - Level - init 1
        - Base Primary attributes
        - Total primary attributes
*/