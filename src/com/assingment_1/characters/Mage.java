package com.assingment_1.characters;

import com.assingment_1.items.Item;
import com.assingment_1.items.Weapon;

public class Mage extends Character{
    public Mage(String name) {
        super(name, 5,1,1,8);
        allowedTypes.add(Item.ITEM_TYPE.CLOTH);
        allowedWeapons.add(Weapon.WEAPON_TYPE.STAFF);
        allowedWeapons.add(Weapon.WEAPON_TYPE.WAND);
    }

    // increment this characters stats by params
    public void levelUp() {
        super.levelUp(3,1,1,5);
    }


    @Override
    public int getPrimaryAttribute() {
        return this.getTotalAttributes().getIntelligence();
    }

    @Override
    public String toString() {
        return "mage";
    }
}