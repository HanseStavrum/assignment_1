package com.assingment_1.characters;

import com.assingment_1.items.Item;
import com.assingment_1.items.Weapon;

public class Ranger extends Character{
    public Ranger(String name) {
        super(name, 8, 1, 7, 1);
        allowedTypes.add(Item.ITEM_TYPE.LEATHER);
        allowedTypes.add(Item.ITEM_TYPE.MAIL);
        allowedWeapons.add(Weapon.WEAPON_TYPE.BOW);
    }

    // increment this characters stats by params
    public void levelUp() {
        super.levelUp(2,1,5,1);
    }

    @Override
    public int getPrimaryAttribute() {
        return this.getTotalAttributes().getDexterity();
    }

    @Override
    public String toString() {
        return "ranger";
    }
}
