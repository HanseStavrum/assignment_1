package com.assingment_1.characters;

import com.assingment_1.items.Item;
import com.assingment_1.items.Weapon;

import java.util.ArrayList;
import java.util.HashMap;

public class Warrior extends Character{

    public Warrior(String name) {
        super(name, 10, 5, 2, 1);
        allowedTypes.add(Item.ITEM_TYPE.MAIL);
        allowedTypes.add(Item.ITEM_TYPE.PLATE);
        allowedWeapons.add(Weapon.WEAPON_TYPE.AXE);
        allowedWeapons.add(Weapon.WEAPON_TYPE.HAMMER);
        allowedWeapons.add(Weapon.WEAPON_TYPE.SWORD);
    }



    // increment this characters stats by params
    public void levelUp() {
        super.levelUp(5,3,2,1);
    }

    @Override
    public int getPrimaryAttribute() {
        return this.getTotalAttributes().getStrength();
    }

}
