package com.assingment_1.items;

import com.assingment_1.attributes.Attributes;

public abstract class Item {
    private String name;
    private int requiredLevel;
    private Attributes attributes;

    private SLOT slot;
    private ITEM_TYPE item_type;

    public Item(SLOT slot, ITEM_TYPE item_type, Attributes attributes, int requiredLevel) {
        this.slot = slot;
        this.item_type = item_type;
        this.attributes = attributes;
        this.requiredLevel = requiredLevel;
    }

    public String getName() {
        return name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public SLOT getSlot() {
        return slot;
    }

    public ITEM_TYPE getItem_type() {
        return item_type;
    }

    public enum SLOT {
        HEAD, BODY, LEGS, WEAPON
    }
    public enum ITEM_TYPE {
        CLOTH, LEATHER, MAIL, PLATE, WEAPON
    }

}


