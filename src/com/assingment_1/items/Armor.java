package com.assingment_1.items;

import com.assingment_1.attributes.Attributes;

public class Armor extends Item {
    public Armor(SLOT slot, ITEM_TYPE armor_type, Attributes attributes, int requiredLevel) {
        super(slot, armor_type, attributes, requiredLevel);
    }
}
