package com.assingment_1.items.exceptions;

public class InvalidWeaponException extends Exception{
    public InvalidWeaponException(String message) {
        super(message);
    }
}