package com.assingment_1.items.exceptions;

public class InvalidArmorException extends Exception{
    public InvalidArmorException(String message) {
        super(message);
    }
}
