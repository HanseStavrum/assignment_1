package com.assingment_1.items;

import com.assingment_1.attributes.Attributes;

public class Weapon extends Item{
    private WEAPON_TYPE weapon_type;
    private int damage;
    private int attacksPerSecond;
    private int dps;
    public Weapon(WEAPON_TYPE weapon_type, int damage, int APS, int requiredLevel) {
        super(SLOT.WEAPON, ITEM_TYPE.WEAPON, new Attributes(0,0,0,0), requiredLevel);
        this.weapon_type = weapon_type;
        this.damage = damage;
        this.attacksPerSecond = APS;
        this.dps = this.damage * attacksPerSecond;
    }

    public enum WEAPON_TYPE {
        AXE, BOW, DAGGER, HAMMER, STAFF, SWORD, WAND;
    }

    public WEAPON_TYPE getWeapon_type() {
        return weapon_type;
    }
    public int getDps() {
        return dps;
    }
}
