package com.assingment_1.attributes;

import org.w3c.dom.Attr;

import java.util.Objects;

public class Attributes {
    private int vitality;
    private int strength;
    private int dexterity;
    private int intelligence;

    public Attributes(int v, int s, int d, int i) {
        this.vitality = v;
        this.strength = s;
        this.dexterity = d;
        this.intelligence = i;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attributes that = (Attributes) o;
        return vitality == that.vitality && strength == that.strength && dexterity == that.dexterity && intelligence == that.intelligence;
    }

    // when unequipping items
    public void removeAttributes(Attributes otherAttributes) {
            int vitMod = otherAttributes.getVitality();
            int strMod = otherAttributes.getIntelligence();
            int dexMod = otherAttributes.getDexterity();
            int intMod = otherAttributes.getIntelligence();
            vitality = vitality - vitMod;
            strength = strength - strMod;
            dexterity = dexterity - dexMod;
            intelligence = intelligence - intMod;
    }

    // on equipping an item
    public void addAttributes(Attributes otherAttributes) {
            int vitMod = otherAttributes.getVitality();
            int strMod = otherAttributes.getStrength();
            int dexMod = otherAttributes.getDexterity();
            int intMod = otherAttributes.getIntelligence();
            vitality = vitality + vitMod;
            strength = strength + strMod;
            dexterity = dexterity + dexMod;
            intelligence = intelligence + intMod;
    }


    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    @Override
    public String toString() {
        return
                "vitality=" + vitality +
                ", strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence;
    }
}
