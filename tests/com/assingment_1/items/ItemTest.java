package com.assingment_1.items;

import com.assingment_1.attributes.Attributes;
import com.assingment_1.characters.Warrior;
import com.assingment_1.items.exceptions.InvalidArmorException;
import com.assingment_1.items.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static com.assingment_1.items.Item.ITEM_TYPE.CLOTH;
import static com.assingment_1.items.Item.ITEM_TYPE.PLATE;
import static com.assingment_1.items.Item.SLOT.HEAD;
import static com.assingment_1.items.Weapon.WEAPON_TYPE.AXE;
import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    void equipArmor_validItem_ShouldIncreaseCharacterItemStats() throws Exception{
        // Arrange
        Armor helm = new Armor(HEAD, PLATE, new Attributes(5,1,0,0), 1);
        Warrior warrior = new Warrior("Borat");
        warrior.equip(helm);
        Attributes expectedItemStats = new Attributes(5, 1, 0, 0); // helm adds 5 vitality

        // Act
        Attributes actualItemStats= warrior.getItemAttributes();

        // Assert
        assertEquals(expectedItemStats, actualItemStats);
    }

    @Test
    void equipArmor_validItem_ShouldIncreaseCharacterTotalStats() throws Exception{
        // Arrange
        Armor helm = new Armor(HEAD, PLATE, new Attributes(5,1,1,1), 1);
        Warrior warrior = new Warrior("Borat");
        warrior.equip(helm);
        Attributes expectedTotalStats = new Attributes(15, 6, 3, 2); // helm adds 5 vitality

        // Act
        Attributes actualTotalStats = warrior.getTotalAttributes();

        // Assert
        assertEquals(expectedTotalStats, actualTotalStats);
    }

    @Test
    void equipArmor_insufficientLevel_ShouldThrowArmorError() throws Exception{
        // Arrange
        Armor helm = new Armor(HEAD, PLATE, new Attributes(5,1,1,1), 2);
        Warrior warrior = new Warrior("Borat");
        Exception exception = assertThrows(InvalidArmorException.class, () -> warrior.equip(helm));
        String expected = "Character is too low level to equip item";
        // Act
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_insufficientLevel_ShouldThrowInvalidArmorException() throws Exception{
        // Arrange
        Armor helm = new Armor(HEAD, PLATE, new Attributes(5,0,0,0), 5);
        Warrior warrior = new Warrior("Borat");
        Exception exception = assertThrows(InvalidArmorException.class, () -> warrior.equip(helm));
        String expected = "Character is too low level to equip item";
        // Act
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_invalidItemTypeForClass_ShouldThrowInvalidArmorException() throws Exception{
        // Arrange
        Armor helm = new Armor(HEAD, CLOTH, new Attributes(1,0,0,0), 1);
        Warrior warrior = new Warrior("Borat");
        Exception exception = assertThrows(InvalidArmorException.class, () -> warrior.equip(helm));
        String expected = "Character cannot equip that Item-Type";
        // Act
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_invalidWeaponTypeForClass_ShouldThrowInvalidWeaponException() throws Exception{
        // Arrange
        Weapon wand = new Weapon(Weapon.WEAPON_TYPE.WAND,1, 1, 1);
        Warrior warrior = new Warrior("Borat");
        Exception exception = assertThrows(InvalidWeaponException.class, () -> warrior.equip(wand));
        String expected = "Character cannot equip that Weapon-Type";
        // Act
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_insufficientLevel_ShouldThrowInvalidWeaponException() throws Exception{
        // Arrange
        Weapon wand = new Weapon(Weapon.WEAPON_TYPE.AXE,1, 1, 2);
        Warrior warrior = new Warrior("Borat");
        Exception exception = assertThrows(InvalidWeaponException.class, () -> warrior.equip(wand));
        String expected = "Character is too low level to equip item";
        // Actual
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_validWeaponType_ShouldReturnTrue() throws Exception{
        // Arrange
        Weapon axe = new Weapon(Weapon.WEAPON_TYPE.AXE,1, 1, 1);
        Warrior warrior = new Warrior("Borat");
        boolean expected = true;

        // Act
        boolean actual = warrior.equip(axe);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_validArmorType_ShouldReturnTrue() throws Exception{
        // Arrange
        Armor helm = new Armor(HEAD, PLATE, new Attributes(0,0,0,0), 1);
        Warrior warrior = new Warrior("Borat");
        boolean expected = true;

        // Act
        boolean actual = warrior.equip(helm);

        // Assert
        assertEquals(expected, actual);
    }

}