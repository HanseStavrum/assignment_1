package com.assingment_1.attributes;

import org.junit.jupiter.api.Test;
import org.w3c.dom.Attr;

import static org.junit.jupiter.api.Assertions.*;

class AttributesTest {

    @Test
    void removeAttributes() {
        // Arrange
        Attributes originalAttributes = new Attributes(5,5,5,5);
        Attributes removeTheseAttributes = new Attributes(1,1,1,1);
        Attributes expected = new Attributes(4, 4 , 4, 4);

        // Act
        originalAttributes.removeAttributes(removeTheseAttributes);
        Attributes actual = originalAttributes;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void addAttributes() {
        // Arrange
        Attributes originalAttributes = new Attributes(5,5,5,5);
        Attributes addTheseAttributes = new Attributes(1,1,1,1);

        // Act
        Attributes expected = new Attributes(6,6,6,6);
        originalAttributes.addAttributes(addTheseAttributes);
        Attributes actual = originalAttributes;

        // Assert
        assertEquals(expected, actual);
    }


}