package com.assingment_1.characters;

import com.assingment_1.attributes.Attributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    @Test
    void levelUp_LeveledToLevelTwo_StatsAreCorrect() {
        // Arrange
        Ranger ranger = new Ranger("Borat"); // very nice
        Attributes expected = new Attributes(10,2,12,2);
        // Act - where we do stuff
        ranger.levelUp();
        Attributes actual = ranger.getBaseAttributes();

        // Assert
        assertEquals(expected, actual);
    }
}