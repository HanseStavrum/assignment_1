package com.assingment_1.characters;

import com.assingment_1.attributes.Attributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    @Test
    void levelUp_LeveledToLevelTwo_StatsAreCorrect() {
        // Arrange
        Mage mage = new Mage("Borat"); // very nice
        Attributes expected = new Attributes(8, 2, 2, 13);
        // Act - where we do stuff
        mage.levelUp();
        Attributes actual = mage.getBaseAttributes();

        // Assert
        assertEquals(expected, actual);
    }
}