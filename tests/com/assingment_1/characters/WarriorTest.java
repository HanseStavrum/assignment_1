package com.assingment_1.characters;

import com.assingment_1.attributes.Attributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    @Test
    void levelUp_LeveledToLevelTwo_StatsAreCorrect() {
        // Arrange
        Warrior warrior = new Warrior("Borat"); // very nice
        Attributes expected = new Attributes(15,8,4,2);
        // Act - where we do stuff
        warrior.levelUp();
        Attributes actual = warrior.getBaseAttributes();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void getPrimaryAttribute_OriginalStartingAttributes_StatsAreCorrect() {
        // Arrange
        Warrior warrior = new Warrior("Borat"); // very nice
        int expected = 5;
        // Act - where we do stuff
        int actual = warrior.getPrimaryAttribute();

        // Assert
        assertEquals(expected, actual);
    }
}