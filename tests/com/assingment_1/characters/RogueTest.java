package com.assingment_1.characters;

import com.assingment_1.attributes.Attributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    @Test
    void levelUp_LeveledToLevelTwo_StatsAreCorrect() {
        // Arrange
        Rogue rogue = new Rogue("Borat"); // very nice
        Attributes expected = new Attributes(11, 3, 10, 2);
        // Act - where we do stuff
        rogue.levelUp();
        Attributes actual = rogue.getBaseAttributes();

        // Assert
        assertEquals(expected, actual);
    }
}