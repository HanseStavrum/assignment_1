package com.assingment_1.characters;

import com.assingment_1.attributes.Attributes;
import com.assingment_1.items.Armor;
import com.assingment_1.items.Item;
import com.assingment_1.items.Weapon;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Attr;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void getLevel_JustBeenCreated_LevelIsOne() {
        Warrior warrior = new Warrior("Borat");
        int expected = 1;
        int actual = warrior.getLevel();
        assertEquals(expected, actual);
    }

    @Test
    void getLevel_LeveledUpOnce_LevelIsTwo() {
        Warrior warrior = new Warrior("Borat");
        warrior.levelUp();
        int expected = 2;
        int actual = warrior.getLevel();
        assertEquals(expected, actual);
    }


    @Test
    void getDps_noEquippedWeapon_ShouldReturnOne() {
        // Arrange
        Warrior warrior = new Warrior("Borat"); // very nice
        double expected = 1.0;

        // Act - where we do stuff
        double actual = warrior.calculateDps();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void getDps_equippedWeapon_ShouldReturnCorrectValue() throws Exception {
        // Arrange
        Warrior warrior = new Warrior("Borat"); // very nice
        Weapon axe = new Weapon(Weapon.WEAPON_TYPE.AXE, 10, 1, 1);
        warrior.equip(axe);
        double expected = 10.5; //10 * (1 + 5/100) -> 10.5

        // Act - where we do stuff
        double actual = warrior.calculateDps();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void getDps_equippedWeaponAndArmor_ShouldReturnCorrectValue() throws Exception {
        // Arrange
        Warrior warrior = new Warrior("Borat"); // very nice
        Weapon axe = new Weapon(Weapon.WEAPON_TYPE.AXE, 10, 1, 1);
        Armor helm = new Armor(Item.SLOT.HEAD, Item.ITEM_TYPE.PLATE, new Attributes(0,5,0,0), 1);
        warrior.equip(axe);
        warrior.equip(helm);
        double expected = 11; //10 * (1 + 5/100) -> 10.5

        // Act - where we do stuff
        double actual = warrior.calculateDps();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void testToString_withoutItems_ShouldReturnCorrrectString() {
        // arrange
        Warrior warrior = new Warrior("Borat");
        String expected = "Character{name='Borat', level=1, vitality=10, strength=5, dexterity=2, intelligence=1, dps=1.0}";

        // act
        String actual = warrior.toString();

        // assert
        assertEquals(expected, actual);
    }

    @Test
    void testToString_withItem_ShouldReturnCorrrectString() throws Exception {
        // arrange
        Warrior warrior = new Warrior("Borat");
        Armor helm = new Armor(Item.SLOT.HEAD, Item.ITEM_TYPE.PLATE, new Attributes(0, 5,0,0), 1);
        warrior.equip(helm);
        String expected = "Character{name='Borat', level=1, vitality=10, strength=10, dexterity=2, intelligence=1, dps=1.0}";

        // act
        String actual = warrior.toString();

        // assert
        assertEquals(expected, actual);
    }
}
