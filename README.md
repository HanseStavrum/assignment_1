Assignment 1 Java RPG Character Creation
The program contains classes that are made to represent a typical 
RPG character, with stats armor and weapons.

The Characters contain name, Attributes, a HashMap containing 
its items. There are 4 classes, Warrior, Mage, Rogue and Ranger.
The Characters inherit traits from the Character-class, and each
can equip different sets of weapons and armor, and they have
unique main-attributes.

The Attributes are the characters stats, and are increased by
leveling up, and equipping armor. The warrior has Strenght as his 
main attribute, the mage Intelligence, and the rogue and ranger have
Dexterity. All characters also recieve more hitpoints from Vitality.

Items are divided into Armor and Weapon. Each item can be equipped
into a specific slot: A helm goes into the HEAD-slot, a weapon into the
WEAPON-slot. Items also have a level requirement.

If the characters fail to meet the requirements for a Weapon or Armor
that they try to equip, they recieve a specific Exception.
If the equipping succeeds, the function simply returns true. 